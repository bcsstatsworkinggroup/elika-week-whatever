# Elika's data (psycholinguistics!)

Create a subdirectory called `data/` and put the data files there (from my email).

# Assignments

Everyone is welcome to try whichever crazy ideas they have!  Here are the assignments for meeting after Thanksgiving (12/5): 

1. Fit polys by subject: **Steve and Keturah**
2. Area under the curve:  **Jennifer**
3. Autocorrelation and bin: **Cory and Dave**
4. Permutation test with cluster correction: **Elika**
5. Playing with random effects structure: **Sarah**
    
## Using git/SourceTree/Bitbucket

### Getting the code

1. Go to the [repository website on Bitbucket](https://bitbucket.org/bcsstatsworkinggroup/elika-week-whatever) (probably already there if you're reading this).
2. Click on the "Clone" button near the top-right of the page.
3. If you haven't installed it yet, [install SourceTree](http://www.sourcetreeapp.com/download/).  Then click "Clone in Sourcetree"
4. This should open up a dialog box in SourceTree where you can pick where to put the code.  If the default is fine, then click "Clone".  Otherwise, click on the little `...` button in the "Destination Path" row, and pick a new destination.  You'll probably want to select an existing directory (like `~/stats-group/`) and then create a new subdirectory.
5. SourceTree will download all the files (but not data).
6. The data have been emailed around previously; create a subfolder of the folder where the code was checked out called `data/` and then save the data files there.  (If you don't know where your code was checked out, you can go to Actions -> Show in Finder which will open up a window showing the checked out code).

### Doing your analysis

1. In SourceTree, click the "Branch" button.
2. Under "New Branch", type your name (or whatever you'd like to call the new branch, like `dave-autocor`).
3. Make sure the "checkout new branch" box is checked, and click "Create Branch".
4. Make changes to the code in Rstudio or wherever.  You might start by editing the existing analysis file, or by creating your own `.R` file if you want to start from scratch (cutting and pasting in the code which reads the data etc.)
5. Commit your changes 
    * After you've made some changes to your code, go back to SourceTree, and click on "Working Copy" on the left bar (under "File Status").  
    * You'll see some files listed under "Files in the working tree" (bottom-left panel). Ones with a yellow elipsis icon are files that git is already aware of that have changed since the last commit; ones with a blue question-mark are files that git is not aware of yet.
    * Drag any files you have changed and want to commit to the "Files staged in the index" pane above.  They should disappear from the bottom panel and appear in the top panel.
    * When you're ready, click on the "Commit" button at the top left.
    * Enter a little message in the dialog box that opens up, about why you made the changes.  The first line should be a short (a few words) summary, with a longer description following a blank line.  For instance,
    
            Updated the README file with git instructions and assignments

            In meeting today we came up with some analyses for people to try, 
            and farmed them out to "volunteers".  I wanted to also provide some
            instructions for how to use the git repository to do individual 
            assignments.
            
    * Click "commit" to, you know, commit! 

6. When you've got something ready to share (may be just one commit worth of work, may be more) push your changes to the shared repository:
    * Click on the "Push" button in the toolbar at the top.
    * If you're pushing for the first time: 
        * Uncheck the "Select All" checkbox at the bottom.
        * Check the box under the "Push" column for your branch.
        * Make sure the "Remote Branch" column shows the same name as your branch, and that the "Track?" box is checked.
    * If you've already pushed this branch at least once, these things should already be done...
    * Click "OK" to push.
    * Because you've been working on a different branch, this shouldn't conflict with anything else people have done, and you should get no errors.  If you *do* get an error, ask Dave or someone else for help :)

### Incorporating other peoples changes into your own code

Let's say someone else has modified the basic analysis code, or has some analysis that you'd like to base your own off of.  Assuming you've already created a branch for your work as described in the last section, you'll want to *merge* the other branch into your own. 

1. In SourceTree, click "Fetch" from the toolbar at the top, and click "OK" to pull down the remote information
2. Click the "Merge" button in the top toolbar. 
3. At the top of the little panel that drops down, click on "Merge Fetched".  
4. Select the branch which has the changes you want from the drop down menu, and click "Okay".
5. If any of the changes affect files that you have also changed, then there may be conflicts.  SourceTree will pop up a dialog telling you this. To resolve conflicts: 
    * Click on a conflicted file in the main SourceTree window (they'll have a yellow diamond caution sign icon)
    * Under Actions -> Resolve Conflicts, select "Resolve Using 'Mine'" (if you want to keep all of your changes and throw out all of theirs), "Resolve Using 'Theirs'" (if you want to throw out all your changes and use their version), or "Launch External Merge Tool" which will open up a separate program (FileMerge) which shows both files side-by-side, highlighting differences and letting you pick which version to use for each difference (use the arrows to navigate from one conflict to the next, and to select the left or right version).  Save the file when your done.
    * If you've used the external merge tool, then you need to drag the conflicted files from the bottom panel to the top panel (as if you're adding them to be committed), and then click Commit.  There will be a pre-specified message about which branch you're merging and the conflicted files; you can add something about how you resolved the conflicts if you want, or just click "Commit".

6. Profit!
