#change this to wherever you put the files)
setwd("~/Desktop/studies/PunStudy_newsample")
punbin<-read.table("punbinOct2012.txt",header=T)
#some steps to make the file more interpretable/smaller
punbin<-subset(punbin, select=-c(MouseC, NounHeard, VerbHeard,noun_onset, soundfile, verb_onset,crit1_comp2T, naming_trialT, Double.x, Competitor.x, Distractor.x, Tname_match, crit1_comp2C, naming_trialC, Double.y, Competitor.y, 
                                Distractor.y, Cname_match,wrongclick,subcondition, VerbStart, clicktime_reltoN))
punbin$SubjectNumber<-as.factor(punbin$SubjectNumber)
punbin$comp_loc<-as.factor(punbin$comp_loc)
punbin$list<-as.factor(punbin$list)
#and now an explanation of the headers
head(punbin)
summary(punbin)

#basic experiment structure: after doing a picture naming task in which subjects named images,
#they did a two-picture eyetracking task where on critical trials there were two images: a double-image that was the competitor
#and a target that had overlapping onset with one of the names of the double, e.g. pillow with pillar/column.
#on other trials there were targets that were frequency matched with the overallping onset word, so, e.g. squirrel with pillar/column
#then there were trials with just two fillers.
#
#SubjectNumber: factors 1:25
#comp_image: what the competitor image was in the trial
#target_image: what the named item in the trial was
#TRIAL_INDEX: trial number, 1-94
#MouseT: what time the mouse click happened
#timeBin: time from start of trial, in 20ms bins
#comp_loc: location of the competitor, 1 or 2 for left, right
#condition: crit for critical trials, filler for filler trials, and freq for frequency matched trials
#list: trial order list 1 or 2
#target_loc: location of the target, 1 or 2, for left, right
#IA: interest area, are they looking at target (1) or distractor (0) or other (NA)
#uptoclick: is this in the part of the trial before the click, yes (Y) or no (N) 
#subcondcoarse: THIS IS THE CRUCIAL CONDITION VARIABLE. 
#####filler=filler trials, freq= frequency matched trials, potential=conceptual match but no phonological overlap (e.g. they called it column and the target is pillow)
#####true= phonological overlap (e.g. they called it pillar and target is pillow), 
#####NAs are for one item which for a couple of subjects had the wrong image file, and so this trial is taken out
#NounStart: where in the trial the noun started (the zero point for plotting things, here it's in 20ms bins)
#propfix: fixation proportion, D=distractor, O=other, T=target
#true_pot: similar to subcondcoarse but collapses true and potential
#diffitems: the double items that the rating study said were different in how good a fit each word was to the picture, coded Y and N for yes and no 


#make nlme and Hmisc libraries available
library(nlme) #for model fitting
library(Hmisc) #for graphing
library(ggplot2)
library(reshape)
library(plyr)

#we're going to fit the curves from 200 after onset of noun to 1000 ms after, distracter looks only
colnames(punbin)
head(punbin)

punbin.countcondsGCA<-count(punbin, c("NounStart", "propfix","subcondcoarse","SubjectNumber"))
punbinsubN_GCA <-subset(punbin.countcondsGCA, NounStart>0 & NounStart<=1000/20 &!is.na(subcondcoarse))

punbinsubN_GCA <-ddply(punbinsubN_GCA, .(NounStart, subcondcoarse, SubjectNumber), transform, percent=freq/sum(freq))
punbinsubN_GCA<-subset(punbinsubN_GCA,propfix=="D" & subcondcoarse!="filler")

punbinsubN_GCA $subcondcoarse<-factor(punbinsubN_GCA $subcondcoarse)
punbinsubN_GCA $SubjectNumber <-factor(punbinsubN_GCA $SubjectNumber)

summary(punbinsubN_GCA)
ncol(punbinsubN_GCA)
colnames(punbinsubN_GCA)

#create 4th order orthogonal polynomial time variable
t<-poly(unique(punbinsubN_GCA$NounStart),4)
dim(t)
dim(punbinsubN_GCA)
#make a data-time sized array of orthogonal times, time adjusted for excluded values
ot<-t[punbinsubN_GCA$NounStart,1:4]

#put the time values into separate variables in the experiment data frame
punbinsubN_GCA $ot1<-ot[,1]
punbinsubN_GCA $ot2<-ot[,2]
punbinsubN_GCA $ot3<-ot[,3]
punbinsubN_GCA $ot4<-ot[,4]

summary(punbinsubN_GCA)
plot(ot[,4])
  ########### COMPETITOR FIXATIONS ###########
#blue and green are both dif from red, duh
library(lme4)
model1<-lmer(percent ~ (ot1+ot2+ot3+ot4)*subcondcoarse + (ot1+ot2+ot3+ot4 |SubjectNumber) +
               (ot1+ot2 | SubjectNumber:subcondcoarse), data=punbinsubN_GCA, REML=F, verbose=T)
summary(model1)
anova(model1)
levels(punbinsubN_GCA$subcondcoarse)
modelsteve<-lmer(percent ~ (ot1+ot2+ot3+ot4)*subcondcoarse + (1|SubjectNumber),
                 data=punbinsubN_GCA, REML=F, verbose=T)

#plotting all 3 lines
punbinfit_allcond<-data.frame(punbinsubN_GCA, fitValsFull=fitted(model1))

punbinfit_allcondSTEVE<-data.frame(punbinsubN_GCA, fitValsFull=fitted(modelsteve))

#plot with fit from model1####
Pun1_subconditions_fittedGCAall<-ggplot(punbinfit_allcondSTEVE , aes(NounStart*20, percent,
                                                                color=subcondcoarse))+
  stat_summary(fun.data="mean_se")+
  stat_summary(fun.y=mean, geom="point", size=3)+
  stat_summary(aes(y=fitValsFull), fun.y=mean, geom="line", size=2)+ 
  theme_bw(base_size=16)+ylab("Prop_Competitor_Fixation")+xlab("Noun Onset")
ggsave(filename="Pun1_subconditions_fittedGCAall.pdf", dpi=300)


#blue is dif from green####
model1b<-lmer(percent ~ (ot1+ot2+ot3+ot4)*subcondcoarse + (ot1+ot2+ot3+ot4 |SubjectNumber) + 
                (ot1+ot2 | SubjectNumber:subcondcoarse), data=subset(punbinsubN_GCA, subcondcoarse!="freq"), REML=F, verbose=T)
summary(model1b)
summary(punbinsubN_GCA)
coefs <- data.frame(summary(model1b)@coefs) 
coefs$p <- 2*(1-pnorm(abs(coefs$t.value)))

model1c<-lmer(percent ~ (ot1+ot2+ot3+ot4)+subcondcoarse + (ot1+ot2+ot3+ot4 |SubjectNumber) + 
                (ot1+ot2 | SubjectNumber:subcondcoarse), data=subset(punbinsubN_GCA, subcondcoarse!="freq"), REML=F)


####model1c the model for potential vs. true different in pun1####
summary(model1c)
anova(model1c, model1b)# model with interaction is better

#plotting
length(fitted(model1c))
mean_se <- function(data){
  y <- mean(data)
  se <- sqrt(var(data)/length(data))
  ymin <- y-se
  ymax <- y+se
  return(data.frame(y,ymin,ymax))
}

punbinfit<-data.frame(punbinsubN_GCA, fitValsFull=fitted(model1b), fitValsNoInteraction=fitted(model1c))
summary(punbinfit)
summary(model1b)
#plot with fit from model 1b####
Pun1_subconditions_fittedGCA<-ggplot(subset(punbinfit, subcondcoarse!="freq") , aes(NounStart*20, percent, color=subcondcoarse))+stat_summary(fun.data="mean_se")+
  stat_summary(fun.y=mean, geom="point", size=3)+stat_summary(aes(y=fitValsFull), fun.y=mean, geom="line", size=2)+ 
  theme_bw(base_size=16)+ylab("Prop_Competitor_Fixation")+xlab("Noun Onset")
ggsave(filename="Pun1_subconditions_fittedGCA.pdf", dpi=300)

#so true vs. pot in pun1 makes a difference; conditions are significantly different

